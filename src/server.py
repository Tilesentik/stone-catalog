import os
import sys
import json
import shutil
import secrets
from os.path import expanduser
from flask import Flask, render_template, request, redirect, url_for, session, send_from_directory


# SET VARIABLES
ALLOWED_EXTENSIONS = {'png'}
app = Flask(__name__)

HOMEDIR = expanduser("~")
SETTINGS_FOLDER = HOMEDIR + "/Latch/S-Catalog/settings/"

# ----------------------------------------------------------#
# INITIAL SETUP                                             #
# ----------------------------------------------------------#

# CREATE APPLICATION FOLDERS
print("== Checking Project Folders")
try: os.mkdir(HOMEDIR + "/Latch")
except: pass
try: os.mkdir(HOMEDIR + "/Latch/S-Catalog")
except: pass
try: os.mkdir(HOMEDIR + "/Latch/S-Catalog/desktop")
except: pass
try: os.mkdir(HOMEDIR + "/Latch/S-Catalog/server")
except: pass
try: os.mkdir(HOMEDIR + '/Latch/S-Catalog/firmware')
except: pass
try: os.mkdir(HOMEDIR + '/Latch/S-Catalog/cloud')
except: pass
try: os.mkdir(HOMEDIR + '/Latch/S-Catalog/settings')
except: pass

# Create config.json file
try: 
    if (os.path.isfile(SETTINGS_FOLDER+"config.json")):
        pass
    else:
        with open(SETTINGS_FOLDER+"config.json", 'w') as f:
            temp = {"username": "stone", "password": "stone"}
            f.write(json.dumps(temp))
except: pass

app.config['UPLOAD_FOLDER'] = HOMEDIR + "/Latch/S-Catalog/"


# NEW PROJECT FUNCTION
def create_project(pDir, pName, pType, pDescr):
    pDir = pDir + pType + "/" + pName
    if (not os.path.exists(pDir)):
        print(pName)
        jsonData = [{
            "name": pName,
            "descr": pDescr,
            'type': pType
        }]
        #print(jsonData)
        os.mkdir(pDir)
        os.mkdir(pDir + '/samples')
        os.mkdir(pDir + '/releases')
        os.mkdir(pDir + '/source')
        print("====" + pDir)
        shutil.copyfile("static/popup/img/ico.png", pDir + "/ico.png")

        with open(pDir + '/info.json', 'w') as x:
            x.write(json.dumps(jsonData))
            print('Saved!')

# CREATE DEFAULT PROJECTS
create_project(HOMEDIR + "/Latch/S-Catalog/", "Sample", "server", "hi")


projectDir = HOMEDIR + "/Latch/S-Catalog/"
servicesFolder = projectDir + "/server"
appsFolder = projectDir + "/desktop"

# ----------------------------------------------------------#
# PRIMARY ROUTES                                            #
# ----------------------------------------------------------#

# GET REQUESTS
@app.route("/")
def index():
    return redirect('/home')

@app.route("/login", methods=["GET", "POST"])
def login():
    if (request.method == 'POST'):
        login_uname = request.form["username"]
        login_passwd = request.form["password"]
        with open(SETTINGS_FOLDER+"config.json") as f:
            data = json.load(f)
        if (login_passwd == data['username']):
            print(data["username"])
            if (login_passwd == data['password']):
                print(data["password"])
                session['uname'] = request.form["password"]
                session['passwd'] = request.form["password"]
                return redirect('/home')
        else:
            return render_template('login/login.html')
    else:
        return render_template('login/login.html')

@app.route("/logout")
def logout():
    session.pop('uname')
    session.pop('passwd')
    return redirect('/login')

@app.route("/home")
def home():
    try: 
        if (session['uname'] and session["passwd"]):
            return render_template("home/home.html")
    except:
        return redirect('/login')

@app.route("/search")
def search():
    return render_template('search/search.html')

@app.route('/banner/<path:filename>')
def banner(filename):
    return send_from_directory(app.config["UPLOAD_FOLDER"], filename)

@app.route("/panel/<item1>/<item2>")
def panel (item1, item2):
    description = ""
    with open(HOMEDIR + "/Latch/S-Catalog/" + item1 + "/" + item2 + '/' + "info.json") as f:
        j = json.load(f)
        description = j[0]["descr"]
        return render_template('panel/panel.html', item1=item1, item2=item2, description=description)
    

# POST REQUESTS
@app.route("/load_projects", methods=['GET', "POST"])
def load_project():
    temp_list = []
    name = request.form['name']
    if (name == "desktop" or name == "server" or name == "firmware"):
        for i in os.listdir(HOMEDIR + "/Latch/S-Catalog/" + name + "/"):
            with open(HOMEDIR + "/Latch/S-Catalog/" + name +"/" + i + '/info.json', 'r') as f:
                json_data = f.read()
            temp_list.append(json.loads(json_data)[0])
        print(temp_list)
    else:
        for i in os.listdir(HOMEDIR + "/Latch/S-Catalog/desktop"):
            if (i == name):
                print(i)
                with open(HOMEDIR + "/Latch/S-Catalog/desktop/" + name + "/info.json", 'r') as f:
                    json_data = f.read()
                temp_list.append(json.loads(json_data)[0])

        for i in os.listdir(HOMEDIR + "/Latch/S-Catalog/server"):
            if (i == name):
                print(i)
                with open(HOMEDIR + "/Latch/S-Catalog/server/" + name + "/info.json", 'r') as f:
                    json_data = f.read()
                temp_list.append(json.loads(json_data)[0])
        
        for i in os.listdir(HOMEDIR + "/Latch/S-Catalog/firmware"):
            if (i == name):
                print(i)
                with open(HOMEDIR + "/Latch/S-Catalog/firmware/" + name + "/info.json", 'r') as f:
                    json_data = f.read()
                temp_list.append(json.loads(json_data)[0])
        
    return temp_list

@app.route('/new_project', methods=["GET", "POST"])
def new_project():
    new_project_name = request.form["name"]
    new_project_descr = request.form["descr"]
    new_project_type = request.form["type"]
    
    create_project(HOMEDIR + "/Latch/S-Catalog/", 
                    new_project_name, 
                    new_project_type, 
                    new_project_descr)

    try:
        sample1 = request.files['sample1']
        sample2 = request.files['sample2']
        sample3 = request.files['sample3']
        
        if (sample1.filename != ''):
            sample1.save(os.path.join(app.config['UPLOAD_FOLDER'], new_project_type + "/" + new_project_name + "/samples/sample1.png"))
        else:
            shutil.copyfile("static/home/img/sample.png", os.path.join(app.config['UPLOAD_FOLDER'], new_project_type + "/" + new_project_name + "/samples/sample1.png"))
        
        if (sample2.filename != ''):
            sample2.save(os.path.join(app.config['UPLOAD_FOLDER'], new_project_type + "/" + new_project_name + "/samples/sample2.png"))
        else:
            shutil.copyfile("static/home/img/sample.png", os.path.join(app.config['UPLOAD_FOLDER'], new_project_type + "/" + new_project_name + "/samples/sample2.png"))
        
        if (sample3.filename != ''):
            sample3.save(os.path.join(app.config['UPLOAD_FOLDER'], new_project_type + "/" + new_project_name + "/samples/sample3.png"))
        else:
            shutil.copyfile("static/home/img/sample.png", os.path.join(app.config['UPLOAD_FOLDER'], new_project_type + "/" + new_project_name + "/samples/sample3.png"))
        
        print(sample1)
    except: pass

    print("hello new project")
    return redirect(url_for("home"))

# ----------------------------------------------------------#
# EXECUTE                                                   #
# ----------------------------------------------------------#
if __name__ == '__main__':
   #app.secret_key = secrets.token_urlsafe(32)
    app.secret_key = "test" 

    app.run(debug=True, host="0.0.0.0", port=1300)
