(function() {
    async function fade() {
      let oppacity = 1;
      let fadeOut = true;
      setInterval(() => {
        if (fadeOut == false) {
          oppacity += 0.01
        } else {
          oppacity -= 0.01
        }
  
        if (oppacity >= 1) {
          fadeOut = true
        } else if (oppacity <=0 ) {
          fadeOut = false
        }
  
        $('.circle').css('opacity', oppacity)
      }, 20);
    }
    $("button").on("click", function() {
      fade()
    })
  })()