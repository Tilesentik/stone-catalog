
// Load Listing
function loadListing(pName, pDescr, pType) {
  let bannerLink = document.createElement("a")
  bannerLink.setAttribute('href', `/panel/${pType}/${pName}`)
  let popularSection = document.getElementsByClassName("popular")[0]
  let banner = document.createElement("div")
  let div = document.createElement("div")
  let img = document.createElement("img")
  let p1 = document.createElement("p")
  let p2 = document.createElement("p")

  banner.setAttribute('class', 'popular__banner')
  banner.setAttribute('data-type', pType)

  img.setAttribute('src', `/banner/${pType}/${pName}/ico.png`)
  img.setAttribute('height', '50')

  p1.setAttribute("class", 'title')
  p2.setAttribute("class", 'descr')

  const p1Text = document.createTextNode(pName);
  const p2Text = document.createTextNode(pDescr);

  p1.appendChild(p1Text)
  p2.appendChild(p2Text)
  
  div.appendChild(img)
  div.appendChild(p1)
  div.appendChild(p2)

  banner.appendChild(div)
  bannerLink.appendChild(banner)
  popularSection.appendChild(bannerLink)
}

// On Search
let a = document.getElementsByClassName('search__input')[0]
a.addEventListener('keypress', function(e) {
  if (e.code == "Enter") {
  $('.popular').empty()
    console.log(this.value)
    $.post("/load_projects", {name: this.value}, function(result) {
      result.forEach(element => {
        console.log(element)
        loadListing(element['name'], element['descr'])
      });
    })
  }
})

let filter__server = document.querySelector('.filter__server')
let filter__desktop = document.querySelector('.filter__desktop')
let filter__firmware = document.querySelector('.filter__firmware')

$(document).on('click', '.popular__banner', function() {
  localStorage.setItem("repoName", $(this).find('.title').text());
  localStorage.setItem("repoType", $(this).attr('data-type'));
  let repoName = $(this).find('.title').text()
  let repoType = $(this).attr('data-type')


  //$.post("/panel/hi", {repoName: repoName, repoType: repoType}, function(result) {})
})

// FILTER BY DESKTOP 
filter__desktop.addEventListener('click', function() {
  $('.popular').empty()
  $.post("/load_projects", {name: "desktop"}, function(result) {
    result.forEach(element => {
      loadListing(element['name'], element['descr'], element['type'])
    });
  })
})

// FILTER BY SERVER
filter__server.addEventListener('click', function() {
  $('.popular').empty()
  $.post("/load_projects", {name: "server"}, function(result) {
    result.forEach(element => {
      loadListing(element['name'], element['descr'], element['type'])
    });
  })
})



// FILTER BY FIRMWARE
filter__firmware.addEventListener('click', function() {
  $('.popular').empty()
  $.post("/load_projects", {name: "firmware"}, function(result) {
    result.forEach(element => {
      loadListing(element['name'], element['descr'], element['type'])
    });
  })
})


$.post("/load_projects", {name: "desktop"}, function(result) {
  result.forEach(element => {
    loadListing(element['name'], element['descr'], element['type'])
  });
})