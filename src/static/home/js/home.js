document.querySelector('.option__1').addEventListener('click', function() { $('.img1').click() })
document.querySelector('.option__2').addEventListener('click', function() { $('.img2').click() })
document.querySelector('.option__3').addEventListener('click', function() { $('.img3').click() })

function sendImg() {
  var file = $('.img1').get(0).files.item(0);
  console.log(file['name'])
  $.ajax({
    type: 'POST',
    url: '/saveImage',
    data: file,
    processData: false,
    contentType: false
  }).done(function(data) { console.log(data) });
}

document.querySelector('.img1').addEventListener('change', function() {
  if (this.files && this.files[0]) {
    var img = document.querySelector('.sample__1');
    img.onload = () => { URL.revokeObjectURL(img.src) }
    img.src = URL.createObjectURL(this.files[0]);
    sendImg()
  }
});

document.querySelector('.img2').addEventListener('change', function() {
  if (this.files && this.files[0]) {
    var img = document.querySelector('.sample__2');
    img.onload = () => { URL.revokeObjectURL(img.src) }
    img.src = URL.createObjectURL(this.files[0]);
    sendImg()
  }
});

document.querySelector('.img3').addEventListener('change', function() {
  if (this.files && this.files[0]) {
    var img = document.querySelector('.sample__3');
    img.onload = () => { URL.revokeObjectURL(img.src) }
    img.src = URL.createObjectURL(this.files[0]);
    sendImg()
  }
});

