let repoName = document.getElementsByClassName("repo__name")[0]
let repoType = document.getElementsByClassName("repo__type")[0]

repoName.textContent = localStorage.getItem('repoName')
repoType.textContent = localStorage.getItem('repoType')
$.post("/load_repo", {name: localStorage.getItem('repoName'), type: localStorage.getItem('repoType')}, function(result) {
    let a = JSON.parse(result);
    console.log(a)
    let repoDescription = document.getElementsByClassName("repo__description")[0] 
    repoDescription.textContent = result
})

function change__sample() {
    let arrowBackward = document.getElementsByClassName('arrow__backward')[0]
    let arrowForward = document.getElementsByClassName('arrow__forward')[0]

    let sample1 = document.getElementsByClassName('sample__1')[0]
    let sample2 = document.getElementsByClassName('sample__2')[0]
    let sample3 = document.getElementsByClassName('sample__3')[0]

    $('.banner').append(`<img class='sample sample__1' src='/banner/${localStorage.getItem('repoType')}/${localStorage.getItem('repoName')}/samples/sample1.png' height=300 style='border-radius: 4px;' />`)
    $('.banner').append(`<img class='sample sample__2' src='/banner/${localStorage.getItem('repoType')}/${localStorage.getItem('repoName')}/samples/sample2.png' height=300 style='border-radius: 4px;' />`)
    $('.banner').append(`<img class='sample sample__3' src='/banner/${localStorage.getItem('repoType')}/${localStorage.getItem('repoName')}/samples/sample3.png' height=300 style='border-radius: 4px;' />`)

    let currentItem = 1;
    arrowBackward.addEventListener('click', function() {
        if (currentItem != 1) {
            currentItem -= 1
        } else {
            currentItem = 3
        }
        console.log(currentItem)
        $('.sample').hide()
        $(`.sample__${currentItem}`).show()

    })

    arrowForward.addEventListener('click', function() {
        if (currentItem != 3) {
            currentItem += 1
        } else {
            currentItem = 1
        }
        console.log(currentItem)
        $('.sample').hide()
        $(`.sample__${currentItem}`).show()
    })
}

change__sample()