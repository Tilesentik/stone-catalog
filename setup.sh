#!/bin/bash

app_setup() {
	echo "Installing for Debian/Ubuntu"
	
	# Debian/Ubuntu
	echo "Installing python"
	sudo apt install python2 -y
	sudo apt install python3 -y

	echo "Installing pip"
	sudo apt install python3-pip
	
	echo "Installing Flask"
	pip3 install flask
	
	echo "Installing Tornado"
	echo "READY!"
	
	echo "=== Starting Dashboard Server =="
	cd src && python3 server.py
}


app_run() {

}

if [ $1 == "-setup" ]; then 
	app_setup
elif [ $1 == "-run" ]; then
	app_run
else
	echo "Error: Unknown Parameter"
fi

