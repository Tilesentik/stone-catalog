# Stone Catalog

Stone Catalog is a project storage tool that allows for lite version control.

# Execution
1. <code>cd</code> into the stone-catalog directory
2. download dependencies with <code>./setup.sh -install</code>
3. run the program with <code>./setup.sh -run</code>
4. the service will begin running on <code>localhost:1300</code>

# Structure / Environment 
Stone Catalog is targeted at Ubuntu Server LTS, but can be modified for any Linux Distro, including Windows and BSD.

## Code Base
- **Python** - Application server code 
- **Bash** - Setup script for installing dependencies and configuring environment
- **HTML/CSS** - Web UI structure and design

## Hierarchy
- **/src** - Location for source code 
- **/src/static** - Location for CSS, JS, and IMG files
- **/src/templates/** - Location for HTML files
